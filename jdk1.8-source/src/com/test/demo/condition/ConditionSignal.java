package com.test.demo.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/2/27 11:21
 */
public class ConditionSignal extends Thread{
    private Lock lock;
    private Condition condition;

    public ConditionSignal(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            System.out.println("开始执行 ConditionSignal");
            condition.signal();
            System.out.println("执行结束 ConditionSignal");
        }finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        ConditionAwait await = new ConditionAwait(lock,condition);
        await.start();
        await.join(10);
        ConditionSignal signal = new ConditionSignal(lock,condition);
        signal.start();
    }
}
