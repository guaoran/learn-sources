package com.test.demo.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/2/27 11:24
 */
public class ConditionAwait extends Thread {
    private Lock lock;
    private Condition condition;

    public ConditionAwait(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            System.out.println("开始执行 ConditionAwait");
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("执行结束 ConditionAwait");
        }finally {
            lock.unlock();
        }
    }
}
